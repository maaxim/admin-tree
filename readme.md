# AdminTree
Дополнительный модуль для admincore.

## Установка:
**composer.json**

```json
{
  "repositories": [
    {
      "type": "git",
      "url": "https://maaxim@bitbucket.org/maaxim/admin-tree.git"
    }
  ]
}
```

```json
{
  "require": {
    "maaxim/admin-tree": "^1.0"
  }
}
```

**console/terminal**

```
composer update
php artisan vendor:publish --tag=admin_tree_config
php artisan vendor:publish --tag=admin_tree_assets --force
```

## Config

```php
return [
    // Заголовок страницы
    'page_title' => 'Страницы',
    // Url страницы
    'page_url' => 'pages',

    // Модель для сохранения структуры/добавления/редактирования страниц
    'model' => maaxim\AdminTree\Models\Page::class,
    // Имя таблицы в БД
    'table_name' => 'pages',

    // Максимальный уровень вложенности
    'max-depth' => '5',
    // Возможность указать кастомную ссылку
    // true - доп. поле 'Ссылка' - обязательно для заполнения
    // false - генерация url из заголовка
    'setHref' => false,

    // Отображение кнопок удалить/редактировать
    'deleteButton' => true,
    'editButton' => true,
];
```

## Создание своей модели
Создаём модель [обычным](https://laravel.com/docs/8.x/eloquent#generating-model-classes) способом,
указываем имя таблицы ``protected $table = 'table-name';``. Обязательно подключить Trait ``maaxim\AdminTree\Classes\Traits\TreeModelFunc``. После указываем модель в config файле.

## Использование
```php
use maaxim\AdminTree\Models\Page; // или ваша модель

function each($items)
{
    foreach ($items as $item) {
        echo "<li>{$item->title}</li>";

        if (count($item->childs) > 0) {
            echo "<ul>";
            each($item->childs);
            echo "</ul>";
        }
    }
}

echo "<ol>";
each(Page::getShort());
echo "<ol>";
```
