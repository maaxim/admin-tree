<li class="dd-item dd3-item" data-id="{{ $item->id }}">
    <div class="dd-handle dd3-handle"></div>
    <div class="dd3-content">
        {{ $item->title }}
        <div class="float-right">
            @if(config('admin-tree.editButton'))
                <a href="{{ route('AdminTreeEdit', ['id' => $item->id]) }}" class="btn btn-primary btn-xs">
                    <i class="fas fa-pencil-alt"></i>
                </a>
            @endif
            @if(config('admin-tree.deleteButton'))
                <button class="btn btn-danger btn-xs nestableDelete" data-id="{{ $item->id }}">
                    <i class="fas fa-trash-alt"></i>
                </button>
            @endif
        </div>
    </div>
    @if(count($item->childs))
        @include('admin-tree::elements.page-group', ['items'=>$item->childs])
    @endif
</li>
