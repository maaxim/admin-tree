<ol class="dd-list">
    @foreach($items as $item)
        @include('admin-tree::elements.page-item', ['item' => $item])
    @endforeach
</ol>
