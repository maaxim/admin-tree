@extends('admin::template')

@section('content')
    <div class="card card-outline card-success" id="AdminTreeEdit">
        <div class="card-body">
            <form @submit.prevent="save">
                <v-input
                        class="form-group"
                        name="title"
                        placeholder="Заголовок"
                        :errors="errors"
                        v-model="data.title"
                ></v-input>

                @if(config('admin-tree.setHref'))
                    <v-input
                            class="form-group"
                            name="href"
                            placeholder="Ссылка"
                            :errors="errors"
                            v-model="data.href"
                    ></v-input>
                @endif

                <div class="btn-group">
                    <button type="submit" class="btn btn-success">
                        <i class="fas fa-check"></i>
                        Сохранить
                    </button>
                    <a href="{{ route('AdminTreePages') }}" class="btn btn-outline-secondary">
                        <i class="fas fa-ban"></i>
                        Отмена
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection
