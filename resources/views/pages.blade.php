@extends('admin::template')

@section('content')
    <div class="card card-outline card-primary">
        <div class="card-header">
            <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target="#pagesModal">
                <i class="fas fa-plus"></i>
                Добавить
            </button>
        </div>
        <div class="card-body">
            <div class="dd" data-max-depth="{{ config('admin-tree.max-depth') }}">
                <ol class="dd-list">
                    @foreach($categories as $item)
                        @include('admin-tree::elements.page-item', ['item' => $item])
                    @endforeach
                </ol>
            </div>
            <div class="mt-4 justify-content-end d-flex">
                <button class="btn btn-success btn-flat treeRestructuring">
                    <i class="fas fa-check"></i>
                    Сохранить
                </button>
            </div>
        </div>
        <div id="AdminTree">
            <div class="modal fade" id="pagesModal" tabindex="-1" role="dialog" aria-labelledby="pagesModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="pagesModalLabel">Добавление</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <v-input
                                    class="form-group"
                                    name="title"
                                    placeholder="Заголовок"
                                    v-model="data.title"
                                    :errors="errors"
                            ></v-input>

                            @if(config('admin-tree.setHref'))
                                <v-input
                                        name="href"
                                        placeholder="Ссылка"
                                        :errors="errors"
                                        v-model="data.href"
                                ></v-input>
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-flat" @click="add">
                                <i class="fas fa-plus"></i>
                                Добавить
                            </button>
                            <button type="button" class="btn btn-secondary btn-flat" data-dismiss="modal">
                                Закрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--        <pre id="nestable-output" style="color: #fff"></pre>--}}
    </div>
@endsection
