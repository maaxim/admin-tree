<?php

Route::get('/', 'Pages@page')->name('AdminTreePages');
Route::post('restructuring', 'Pages@restructuring')->name('AdminTreeRestructuring');
Route::post('add', 'Pages@add')->name('AdminTreeAdd');

Route::get('edit', 'Pages@editPage')->name('AdminTreeEdit');
Route::post('edit', 'Pages@edit');

Route::post('delete', 'Pages@delete')->name('AdminTreeDelete');
