<?php

namespace maaxim\AdminTree\Database\Factories;

use Illuminate\Support\Collection;
use maaxim\AdminUrlGenerator\UrlGenerator;
use Illuminate\Database\Eloquent\Factories\Factory;

class PageFactory extends Factory
{
    public function __construct($count = null, ?Collection $states = null, ?Collection $has = null, ?Collection $for = null, ?Collection $afterMaking = null, ?Collection $afterCreating = null, $connection = null)
    {
        parent::__construct($count, $states, $has, $for, $afterMaking, $afterCreating, $connection);
        $this->model = config('admin-tree.model');
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->name;

        return [
            'title' => $name,
            'href' => UrlGenerator::generate($name)
        ];
    }
}
