<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('admin-tree.table_name'), function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('href')->nullable();
            $table->integer('tree')->nullable()->default(0);
            $table->integer('short')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('admin-tree.table_name'));
    }
}
