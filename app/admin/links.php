<?php

Link::add()
    ->name(config('admin-tree.page_title'))
    ->icon('fas fa-layer-group')
    ->active('AdminTreeEdit')
    ->setPageRules('AdminTree', config('admin-tree.page_title'),
        (new \maaxim\admincore\Classes\Roles\PageRule())
            ->setCustomRule('restructuring', false, 'Изменение структуры')
    )
    ->route('AdminTreePages');
