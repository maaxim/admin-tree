<?php

namespace maaxim\AdminTree\Models;

use Illuminate\Database\Eloquent\Model;
use maaxim\AdminTree\Classes\Traits\TreeModelFunc;
use maaxim\AdminTree\Database\Factories\PageFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Page extends Model
{
    use TreeModelFunc, HasFactory;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = config('admin-tree.table_name');
    }

    protected static function newFactory()
    {
        return PageFactory::new();
    }
}
