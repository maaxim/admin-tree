<?php

namespace maaxim\AdminTree\Providers;

use maaxim\admincore\Facades\Link;
use maaxim\admincore\Facades\Admin;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/admin-tree.php', 'admin-tree');
        $this->app->registerDeferredProvider(RouteServiceProvider::class);

        $this->publishes([
            __DIR__ . '/../../config/admin-tree.php' => config_path('admin-tree.php'),
        ], 'admin_tree_config');

        $this->publishes([
            __DIR__ . '/../../resources/assets/admin-tree' => public_path(config('admin.resource_path') . '/admin-tree')
        ], 'admin_tree_assets');
    }

    public function boot()
    {
        Admin::setResourceJs(config('admin.resource_path') . '/admin-tree/js/app.js');
        Admin::setResourceCss(config('admin.resource_path') . '/admin-tree/css/app.css');

        Link::includeMappingFile(__DIR__ . '/../admin/links.php');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'admin-tree');
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        Admin::setResourcePath(__DIR__ . '/../../resources/assets/admin-tree');
        Admin::setLanguage(__DIR__ . '/../../resources/lang/ru.json');
    }
}
