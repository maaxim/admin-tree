<?php

namespace maaxim\AdminTree\Classes\Traits;

trait TreeModelFunc
{
    public static function getShort(int $tree = 0)
    {
        return config('admin-tree.model')::orderBy('short')->where('tree', $tree)->get();
    }

    public function childs()
    {
        return $this->hasMany(config('admin-tree.model'), 'tree', 'id');
    }

    public function getChilds($parentId)
    {
        return config('admin-tree.model')::where('tree', $parentId)->orderBy('short')->get();
    }
}
