<?php

namespace maaxim\AdminTree\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Page extends Controller
{
    protected $_request;

    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function get()
    {
        $item = config('admin-tree.model')::findOrFail($this->_request->input('id'));

        return ($this->_request->expectsJson()
            ? response()->json($item)
            : $item
        );
    }
}
