<?php

namespace maaxim\AdminTree\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageUrl extends Controller
{
    protected $_request;

    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function get()
    {
        return ($this->_request->expectsJson()
            ? response()->json(config('admin-tree.page_url'))
            : config('admin-tree.page_url')
        );
    }
}
