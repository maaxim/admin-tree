<?php


namespace maaxim\AdminTree\Http\Controllers;

use Illuminate\Http\Request;
use maaxim\AdminTree\Models\Page;
use App\Http\Controllers\Controller;
use maaxim\AdminUrlGenerator\UrlGenerator;

class Pages extends Controller
{
    protected $_request;

    /**
     * @var Page
     */
    protected $_model;

    public function __construct(Request $request)
    {
        $request->merge(['page' => $this->getPage()]);
        $this->middleware('noAccept');

        $this->_request = $request;
        $this->_model = config('admin-tree.model');
    }

    protected function getPage()
    {
        if (app('router')->currentRouteNamed('AdminTreePages')) $page = 'AdminTree';
        elseif (app('router')->currentRouteNamed('AdminTreeRestructuring')) $page = 'AdminTree.restructuring';
        elseif (app('router')->currentRouteNamed('AdminTreeAdd')) $page = 'AdminTree.add';
        elseif (app('router')->currentRouteNamed('AdminTreeEdit')) $page = 'AdminTree.edit';
        elseif (app('router')->currentRouteNamed('AdminTreeDelete')) $page = 'AdminTree.delete';
        else $page = 'AdminTree';

        return $page;
    }

    public function page()
    {
        return view('admin-tree::pages', [
            'title' => config('admin-tree.page_title'),
            'categories' => $this->_model::getShort()
        ]);
    }

    public function restructuring()
    {
        return response()->json($this->update($this->_request->input('data')));
    }

    protected function update(array $data, $parentId = 0)
    {
        foreach ($data as $key => $item) {
            $get = $this->_model::find($item['id']);
            $get->short = $key;
            $get->tree = $parentId;
            $get->save();

            if (isset($item['children']) && is_array($item['children'])) {
                $this->update($item['children'], $item['id']);
            }
        }

        return true;
    }

    public function add()
    {
        if ($this->_request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255|unique:' . config('admin-tree.table_name'),
                'href' => ''
            ];

            if (config('admin-tree.setHref')) {
                $rules['href'] = 'required|max:255|url';
            }

            $this->validate($this->_request, $rules);

            $model = config('admin-tree.model');
            $new = new $model();
            $new->title = $this->_request->input('title');
            if (config('admin-tree.setHref')) {
                $new->href = $this->_request->input('href');
            } else {
                $new->href = UrlGenerator::generate($this->_request->input('title'));
            }
            $new->tree = 0;
            $new->short = config('admin-tree.model')::count();
            $new->save();

            return response()->json([
                'status' => true,
                'url' => route('AdminTreePages')
            ]);
        }
    }

    public function editPage()
    {
        return view('admin-tree::edit', [
            'title' => 'Редактирование'
        ]);
    }

    public function edit()
    {
        if ($this->_request->isMethod('post')) {
            $rules = [
                'title' => 'required|max:255'
            ];
            $page = config('admin-tree.model')::findOrFail($this->_request->input('id'));

            if ($this->_request->input('title') != $page->title) {
                $rules['title'] .= '|unique:' . config('admin-tree.table_name');
            }

            if (config('admin-tree.setHref')) {
                $rules['href'] = 'required|max:255';
            }

            $this->validate($this->_request, $rules);

            $page->title = $this->_request->input('title');
            if (config('admin-tree.setHref')) {
                $page->href = $this->_request->input('href');
            } else {
                $page->href = UrlGenerator::generate($this->_request->input('title'));
            }
            $page->save();

            return response()->json([
                'status' => true,
                'url' => route('AdminTreePages')
            ]);
        }
    }

    public function delete()
    {
        $page = config('admin-tree.model')::findOrFail($this->_request->input('id'));
        $sub_pages = config('admin-tree.model')::where('tree', $page->id)->get();

        if (!empty($sub_pages->toArray())) {
            foreach ($sub_pages as $item) {
                $sub_page = config('admin-tree.model')::find($item['id']);
                $sub_page->tree = 0;
                $sub_page->save();
            }
        }

        $page->delete();

        return response()->json(true);
    }
}
