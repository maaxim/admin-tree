<?php

return [
    'page_title' => 'Страницы',
    'page_url' => 'pages',

    'model' => maaxim\AdminTree\Models\Page::class,
    'table_name' => 'pages',

    'max-depth' => '5',
    'setHref' => false,

    'deleteButton' => true,
    'editButton' => true,
];
